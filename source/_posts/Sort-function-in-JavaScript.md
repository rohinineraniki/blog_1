---
title: Sort function in JavaScript
date: 2023-03-06 22:34:19
tags:
---

# Blog on Sort method in JavaSript

The sort() is a method it sorts the elements in an array and returns sorted array. Sort() method compares the values in array by using UTF-16(Unicode Transformation Format).The default sorting is ascending order.

### Sort method in string

Take a simple array for example

Example-1

```javascript
let cities = ["Hyderabad", "Bengaluru", "Mumbai", "Chennai", "Delhi"];
cities.sort();
console.log(cities);

//Output
// [ 'Bengaluru', 'Chennai', 'Delhi', 'Hyderabad', 'Mumbai' ]
```

In example-1 the output we got as expected.

Example-2

```javascript
let letters = ["H", "c", "r", "g", "a"];
letters.sort();
console.log(letters);

//Output
//[ 'H', 'a', 'c', 'g', 'r' ]
```

By seeing example-2 we have to think what is happening in sort method here, sort method by default takes values in array converts into string and compare UTF-16 values.

```javascript
const unicode_values = letters.map((each) => {
  return each.charCodeAt();
});
console.log(unicode_values);

//Output
// [ 72, 99, 114, 103, 97 ]
```

For ["H", "c", "r", "g", "a"] array unicode values are [ 72, 99, 114, 103, 97 ] so according to unicode values it will sorted. This happens for numbers also, lets see one example

### Sort method in numbers

Example-3

```javascript
let numbers = [8, 5, 9, 100, 201];
numbers.sort();
console.log(numbers);

//Output
// [ 100, 201, 5, 8, 9 ]
```

Here sort method converts all numbers to strings and then compared by unicode values.

```javascript
let unicode_values = numbers.map((each) => {
  return String(each).charCodeAt();
});

console.log(unicode_values);
// Output
//[ 56, 53, 57, 49, 50 ]
```

To overcome this issue we have compare function in sort method.

### Sort method in strings with compare function

Lets see one example,

Example-1

```javascript
let strings = ["red", "blue", "white", "green"];
let strings_different_cases = ["red", "blue", "WHITE", "Green"];
function compare_strings(string_1, string_2) {
  if (string_1 > string_2) {
    return 1;
  }
  if (string_1 < string_2) {
    return -1;
  }
  return 0;
}

strings.sort(compare_strings);
strings_different_cases.sort(compare_strings);

console.log(strings); // output = [ 'blue', 'green', 'red', 'white' ]
console.log(strings_different_cases); // output = [ 'Green', 'WHITE', 'blue', 'red' ]
//Output
```

Here we taken strings array and sorted by using compare function, here compare_strings function takes 2 array values and compares.But problem remains same if we taken different cases. To overcome this we have to convert each value to either lowercase or upper case. Then sort output is as like expected.

### Sort method in strings with different cases with compare function

Example-2

```javascript
let strings_different_cases = ["red", "blue", "WHITE", "Green"];
function compare_strings(string_1, string_2) {
  let string_1_lower = string1.toLowerCase();
  let string_2_lower = string2.toLowerCase();
  if (string_1_lower > string_2_lower) {
    return 1;
  }
  if (string_1_lower < string_2_lower) {
    return -1;
  }
  return 0;
}

strings_different_cases.sort(compare_strings);
console.log(strings_different_cases); // output = [ 'blue', 'Green', 'red', 'WHITE' ]
```

### Sort method in strings by localeCompare()

For sorting strings with non-ASCII characters, and i.e. strings with accented characters (e, é, è, a, ä, etc.), strings from languages other than English we can use localeCompare() method.

We can also use to sort numbers,strings etc.,

Example-1

```javascript
let strings_different_cases = ["red", "blue", "WHITE", "Green"];
function compare_strings(string1, string2) {
  return string1.localeCompare(string2);
}
strings_different_cases.sort(compare_strings);
console.log(strings_different_cases); // output = [ 'blue', 'Green', 'red', 'WHITE' ]
```

Here we simply getting sorted output without any case conversion.

### Sort method in Numbers

Example-1

```javascript
let numbers = [100, 5, 9, 60, 55];
function compare_numbers(number_1, number_2) {
  return number_1 - number_2;
}
numbers.sort(compare_numbers);
console.log(numbers); // output = [ 5, 9, 55, 60, 100 ]
```

In above example sort numbers array in ascending order. Function "compare_numbers" is used to get sorted array.We can also sort by decending order also.

Example-2

```javascript
let numbers = [100, 5, 9, 60, 55];
function compare_numbers(number_1, number_2) {
  return number_2 - number_1;
}
numbers.sort(compare_numbers);
console.log(numbers); // output = [ 5, 9, 55, 60, 100 ]
```

In the above example we have sorted in decending order.

In all cases original array itself is changing when sorting is done. If original array need not to change and want sorted output in different array we can create copy of original array and apply sort method to copied one. Like shown below,

Example-3

```javascript
let numbers = [100, 5, 9, 60, 55];
function compare_numbers(number_1, number_2) {
  return number_2 - number_1;
}
const sorted_array = [...numbers].sort(compare_numbers);
console.log(sorted_array); // output = [ 5, 9, 55, 60, 100 ]
console.log(numbers); // output = [ 100, 5, 9, 60, 55 ]
```

### Sort method in Objects

Objects contain properties and values, sort method compare value of one of their properties.

Example-1

```javascript
let items = [
  {
    name: "Mango",
    price: 40.5,
  },
  {
    name: "Apple",
    price: 105.68,
  },
  {
    name: "Banana",
    price: 33.58,
  },
  {
    name: "Sapota",
    price: 55,
  },
];

function compare_object(item_1, item_2) {
  let price_1 = item_1.price;
  let price_2 = item_2.price;
  if (price_1 > price_2) {
    return 1;
  }
  if (price_1 < price_2) {
    return -1;
  }
  return 0;
}

items.sort(compare_object);
console.log(items);

// output
[
  { name: "Banana", price: 33.58 },
  { name: "Mango", price: 40.5 },
  { name: "Sapota", price: 55 },
  { name: "Apple", price: 105.68 },
];
```

Above example sorted items array by price value.

### Sort methos in multiple keys in an object

Example-1

```javascript
let items = [
  {
    id: 1,
    first_name: "Gregg",
    last_name: "Lacey",
  },
  {
    id: 2,
    first_name: "Gregg",
    last_name: "Scargill",
  },
  {
    id: 3,
    first_name: "Loren",
    last_name: "Doull",
  },
  {
    id: 4,
    first_name: "Sherwood",
    last_name: "Adriano",
  },
  {
    id: 5,
    first_name: "Addison",
    last_name: "Dofty",
  },
];

function compare_name(name_1, name_2) {
  let first_name_1 = name_1.first_name.toLowerCase();
  let first_name_2 = name_2.first_name.toLowerCase();
  let last_name_1 = name_1.last_name.toLowerCase();
  let last_name_2 = name_2.last_name.toLowerCase();
  if (first_name_1 > first_name_2) {
    return 1;
  }
  if (first_name_1 < first_name_2) {
    return -1;
  }
  if (first_name_1 === first_name_2) {
    if (last_name_1 > last_name_2) {
      return 1;
    }
    if (last_name_1 < last_name_2) {
      return -1;
    }
    return 0;
  }
}

items.sort(compare_name);
console.log(items);

//Output
// [
//   { id: 5, first_name: 'Addison', last_name: 'Dofty' },
//   { id: 1, first_name: 'Gregg', last_name: 'Lacey' },
//   { id: 2, first_name: 'Gregg', last_name: 'Scargill' },
//   { id: 3, first_name: 'Loren', last_name: 'Doull' },
//   { id: 4, first_name: 'Sherwood', last_name: 'Adriano' }
// ]
```

This example initially sorting is done by using key called "first_name" when both key values are have same value then we need sorting for key "last_name".
In output we can observe that when first_name is equal i.e. "Gregg" is sorted by last_name.
