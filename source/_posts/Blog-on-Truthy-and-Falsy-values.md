---
title: Blog on Truthy and Falsy values
date: 2023-02-24 17:40:53
tags:
---

### In this blog you can learn about TRUTY and FALSY values.

Boolean is one of the primitive data type in JavaScript it can be either **true** or **false**.
(We also have Boolean Constructor which is object.)

If a value can be converted to true, the value is so called truthy. If a value can be converted to false, the value is so called falsy. First we go for falsy values so other than falsy values we left truthy values.Falsy values can be false in boolean context.List of falsy values are,

- false - The keyword false.
- 0 - The Number zero (so, also 0.0, etc., and 0x0).
- -0 - The Number negative zero (so, also -0.0, etc., and -0x0).
- 0n - The BigInt zero (so, also 0x0n). Note that there is no BigInt negative zero — the negation of 0n is 0n.
- "", '', `` - Empty string value.
- null - the absence of any value.
- undefined - the primitive value.
- NaN - not a number.

In conditional statements (example:if-statements) and in for loops and where a block of code is need to execute, In these situations usage of truthy and falsy plays main role.

Falsy values in if condition examples:

`if (false) {
    // This block of code not execute
}`

`if (null) {
    // This block of code not execute
}`

`if (undefined) {
    // This block of code not execute
}`

`if (0) {
    // This block of code not execute
}`

`if (-0) {
    // This block of code not execute
}`

`if (0n) {
    // This block of code not execute
}`

`if (NaN) {
    // This block of code not execute
}`

`if ("") {
    // This block of code not execute
} `

Truthy values examples :

`if (5){
    //This block of code executes
}`

`if({}){
    //This block of code executes
}`

`if([]){
    //This block of code executes
}`

In place of condition we can give strings,positive values,negative values(except 0,-0) and empty object,empty array etc.,all these are truthy values. The code written under this block will executes.

In this table take a look how strict equal to (===) gives an output for falsy values.

| ===       | true  | false | 0     | ""    | null  | undefined | NaN   | Infinity |
| --------- | ----- | ----- | ----- | ----- | ----- | --------- | ----- | -------- |
| true      | true  | false | false | false | false | false     | false | false    |
| false     | false | true  | false | false | false | false     | false | false    |
| 0         | false | false | true  | false | false | false     | false | false    |
| " "       | false | false | false | true  | false | false     | false | false    |
| null      | false | false | false | false | true  | false     | false | false    |
| undefined | false | false | false | false | false | true      | false | false    |
| NaN       | false | false | false | false | false | false     | false | false    |
| Infinity  | false | false | false | false | false | false     | false | true     |

In this table take a look how loose equal to (===) gives an output for falsy values.

| ==        | true  | false | 0     | ""    | null  | undefined | NaN   | Infinity |
| --------- | ----- | ----- | ----- | ----- | ----- | --------- | ----- | -------- |
| true      | true  | false | false | false | false | false     | false | false    |
| false     | false | true  | true  | true  | false | false     | false | false    |
| 0         | false | true  | true  | true  | false | false     | false | false    |
| " "       | false | true  | true  | true  | false | false     | false | false    |
| null      | false | false | false | false | true  | true      | false | false    |
| undefined | false | false | false | false | true  | true      | false | false    |
| NaN       | false | false | false | false | false | false     | false | false    |
| Infinity  | false | false | false | false | false | false     | false | true     |

The only exception is **NaN**, which remains stubbornly **inequivalent** to everything.
