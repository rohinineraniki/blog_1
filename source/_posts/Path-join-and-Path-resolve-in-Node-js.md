---
title: Path.join() and Path.resolve() in Node.js
date: 2023-03-15 15:23:51
tags:
---

### Path Module

The Path module provides utilities for working with file and directory paths,it can be use in our code by including this line

```javascript
const path = require("path");
```

### path.join()

path.join() this method takes path segments as string as an argument. It joins all the path segments and provides normalized path as output. Zero length path segments are ignored. If the joined path string is a zero length string then '.' will be returned, representing the current working directory.It returns string output. A TypeError is thrown if any of the arguments is not a string.

Take a look of examples

```javascript
const path1 = path.join("user/projects/files", "index.js");
console.log(path1);

//   user/projects/files/index.js
```

path module combines directory path "user/projects/files" with name.js file. We can use this path for reading file or write file in fs module.

```javascript
const path1 = path.join(__dirname, "index.js"); //assume cwd is user/projects/files
console.log(path1);

//output
//   user/projects/files/index.js
```

\_\_dirname gives current working directory. path.join() joins current working directory with index.js file.

```javascript
const path1 = path.join(__dirname, ".."); //assume cwd is user/projects/files
console.log(path1);

//output
// user/projects
```

It gives parent directory of present working directory as output path.

```javascript
const path1 = path.join(__dirname, "."); //assume cwd is user/projects/files
console.log(path1);

//output
// user/projects/files
```

It gives current working directory.

```javascript
const path1 = path.join("users", "..", "files", "index.js");
console.log(path1);

//output
//files/index.js
```

path.join() goes left to right first it goes to "users" folder then "..", i.e.,parent folder means now we are in current directory then it moves to files/index.js.

```javascript
const path1 = path.join("users", "..");
console.log(path1);

//output
// .
```

It goes to users folder and joining with ".." so it is coming back again to current working directory so output is only .(dot)

### path.resolve()

The path.resolve() method resolves a sequence of paths or path segments into an absolute path. it takes string as an arguments. It also returns string output.A TypeError is thrown if any of the arguments is not a string.

```javascript
const path1 = path.resolve("users", "/project", "files");
console.log(path1);

//   output
//   /project/files
```

How to get resolved path?

1. Start right to left in given string path sequence
2. Search for an absolute path means when it encounters / it is absolute path.
3. above example after "files" "/project" is absolute path so path.resolve() method resolves a sequence of paths to absolute path.

```javascript
const path1 = path.resolve("users/project", "files");
console.log(path1);

//output
// /home/rohini/users/project/files
```

In Above example users/project adds to current working directory

```javascript
const path1 = path.resolve("/users/project", "files");
console.log(path1);

//   output
//   /users/project/files
```

In this /users/project creates absolute path.

```javascript
const path1 = path.resolve("users", "project", "files");
console.log(path1);

//   output
//   /home/rohini/users/project/files
```

If, after processing all given path segments, an absolute path has not yet been generated, the current working directory is used to generate path.
In above example does not have any absolute paths so it addes to the current working directory.

```javascript
const path1 = path.resolve("users", "/files", "..", "index.js");
console.log(path1);

//   output
//   /index.js
```

To get path of above example follow steps
First it goes to index.js next ".." next "/files" it encounters absolute path so it starts joining from right to left parent directory of files is root(/) so index.js will added to root, output is "/index.js"

```javascript
const path1 = path.resolve("users", "/files", "project", "..", "index.js");
console.log(path1);

//   output
//   /files/index.js
```
