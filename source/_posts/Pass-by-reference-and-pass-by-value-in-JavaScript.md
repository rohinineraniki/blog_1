---
title: Pass by reference and pass by value in JavaScript
date: 2023-03-02 12:39:06
tags:
---

This blog is related to pass by reference and pass by value in javascript.

Generally in javascript we deal with functions, and pass arguments to that functions it can be done in 2 ways it is may be pass by reference and pass by value

Pass by value:

```javascript
let x = 10;

function test(variable) {
  variable += 1;
  console.log(variable);
}

console.log(`Before calling function: ${x}`);
test(x);
console.log(`After calling function: ${x}`);

//output
// Before calling function: 10
// 11
// After calling function: 10
```

It is example of pass by value, here argument we are passing for test function is only value of x not its memory location actually so after function calling the value of x is not changing. In pass by value, values of varible not changes outside the function

```javascript
let object1 = { name: "Ramu", skill: "JavaScript" };

function test(variable) {
  variable.skill = "Python";
  console.log(variable);
}

console.log(`Before calling function: ${object1.skill}`);
test(object1);
console.log(`After calling function: ${object1.skill}`);

//output
// Before calling function: JavaScript
// { name: 'Ramu', skill: 'Python' }
// After calling function: Python
```

By observing above examples we can see that the key called "skill" changed within function but when we console after calling function object1 values are changed. This process we can say as pass by reference

In pass by reference when we pass objects as arguments it takes address of the memory as reference not only the value. In pass by reference, the values of varibles outside the function changes by changing inside of the function.It does not create a copy, instead, it works on the original variable, so all the changes made inside the function affect the original variable as well.

We want one solution that when happens this 2 cases?

For all primitive data types in JavaScript supports pass by value for example string,number,boolean,undefined,null.The original value and the copied value are independent of each other as they both have a different space in memory i.e., on changing the value inside the function, the variable outside the function is not affected.

```javascript
let number1 = 50;
let number2 = 50;
console.log(number1 === number2);

//output
//true
```

For all non primitive date types in JavaScript supports pass by reference for example objects.Unlike pass by value in JavaScript, pass by reference in JavaScript does not create a new space in the memory, instead, we pass the reference/address of the actual parameter, which means the function can access the original value of the variable. Thus, if we change the value of the variable inside the function, then the original value also gets changed.

```javascript
let object1 = { name: "Ramu", skill: "JavaScript" };
let object2 = { name: "Ramu", skill: "JavaScript" };
console.log(object1 === object2);

//output
//false
```

Objects and arrays are not compared by value. That means even if two objects and arrays have the same values and properties or the same elements, respectively, they are not strictly equal. The objects,arrays are equal only when the referred same object.

```javascript
let object1 = { name: "Ramu", skill: "JavaScript" };
let object2 = object1;
console.log(object1 === object2);
//ouput
//true
```
