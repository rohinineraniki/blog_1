---
title: Promises in JavaScript
date: 2023-03-14 12:46:01
tags:
---

### What is Promise

JavaScript promises are very important feature when our code have asynchronous functions.

A promise is an object that may produce a single value some time in the future it represent the eventual completion or failure of an asynchronous operation

### How Promises work

A promise object can be returned synchronously from asynchronous function.A Promise is in one of the three states they are

1. Pending state : It is neither fulfilled nor rejected
2. Fulfilled: The operation was completed successfully then that result we can pass by using **resolve(value)** method

3. Rejected: The operation was failed it will go to rejected state, then that result we can pass by using **reject(error)** method

A Promise object should call only one resolve or one reject. Any further calls to resolve or reject will be ignored.

### How to handle promise

We use **.then()** , **.catch()** handler methods

- **.then()** method executed when resolve() is called. It takes two arguments, the first argument is a callback function for the fulfilled case of the promise and second is a callback function for rejected case.
- **.catch()** method executed when reject(error) is called. It handles with error occurs in asynchronous function. Ending all promise chains with a .catch() is recommended. One example of promise object.

```javascript
const promise = new Promise((resolve, reject) => {
  if (true) {
    resolve("Success");
  } else {
    reject(error);
  }
});
promise.then((data) => console.log(data)).catch((error) => console.log(error));

// Success
```

### Static methods

The promise class offers four static methods to deal with async function

1.  Promise.all()

The promise.all() static method takes an array of promises as input and returns a single promise. The returned promise fulfills when all of the inputs promises fulfill, It rejects when any of the inputs promises rejects.

It is typically used when there are multiple related asynchronous taskes that the overall code relies on to work successfully i.e if the taskes are dependent on each other.

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise1");
  }, 1000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise2");
  }, 2000);
});

Promise.all([promise1, promise2]).then((values) => console.log(values));

//[ 'Done promise1', 'Done promise2' ]
```

Take an example of two async functions which given in promise object both are resolved, so when array of both promises passed to Promise.all() then output containes of data of both promises.

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise1");
  }, 1000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Error");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise3");
  }, 1000);
});

Promise.all([promise1, promise2, promise3]).then((values) =>
  console.log(values)
);

//The promise rejected with the reason "Error"
```

In above example one promise returned reject() method so it need to handle with catch block, and Promise.all() also rejected. It does not executed any fulfilled states. It will reject immediately upon any of the input promises rejecting.

2. Promise.allSettled()

The promise.allSettled() static method takes an array of promises as input and returns a single promise. The returned promise fulfills when all of the inputs promises settle.

We can use Promise.allSettled() method like to know result of each promise, and when we have multiple asynchronous tasks that are not dependent on one anthoer to complete successfully.

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise1");
  }, 1000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done primise2");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise3");
  }, 1000);
});

Promise.allSettled([promise1, promise2, promise3]).then((values) =>
  console.log(values)
);
//Output
/* 
[
  { status: 'fulfilled', value: 'Done promise1' },
  { status: 'fulfilled', value: 'Done primise2' },
  { status: 'fulfilled', value: 'Done promise3' }
]
*/
```

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise1");
  }, 1000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Error");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise3");
  }, 1000);
});

Promise.allSettled([promise1, promise2, promise3]).then((values) =>
  console.log(values)
);

//Output
/*
[
  { status: 'fulfilled', value: 'Done promise1' },
  { status: 'rejected', reason: 'Error' },
  { status: 'fulfilled', value: 'Done promise3' }
]
*/
```

When we use allSettled() method, if any promise rejects it returns that object, along with fulfilled objects. If all promises are called resolve() then it gives output of all fulfilled states.

3. Promise.any()

The Promise.any() static method takes an array of promises as input and returns a single promise. If any one of the promises are fullfills in given promise array, Promise.any() methods returns it immediatly. So it does not wait for the other promises to get fulfilled.

This can be beneficial if we need only one promise to fulfill but we do not care which one does.

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise 1");
  }, 1000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise 2");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise 3");
  }, 1000);
});

Promise.any([promise1, promise2, promise3]).then((values) =>
  console.log(values)
);
//Output
//Done promise 1
```

In above example all promises are in resolved state, we use Promise.any() method so it returns first fullfilled value.

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Error");
  }, 1000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Error");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Error");
  }, 1000);
});

Promise.any([promise1, promise2, promise3]).then((values) =>
  console.log(values)
);

//output
/*
[AggregateError: All promises were rejected] {
  [errors]: [ 'Error', 'Error', 'Error' ]
}
*/
```

We get output like this when all promises are in reject state. Here Promise.all() method returns array of errors.

4. Promise.race()

Promise.race() it waits until any of the promises is fulfilled or rejected. It returns first promise whether that promise is resolves or rejects

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise1");
  }, 4000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done primise2");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise3");
  }, 1000);
});

Promise.race([promise1, promise2, promise3]).then((values) =>
  console.log(values)
);
//output
//Done promise3
```

Here promise3 executes first so we get output as "Done promise3"

```javascript
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done promise1");
  }, 4000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Done primise2");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Done promise3");
  }, 1000);
});

Promise.race([promise1, promise2, promise3]).then((values) =>
  console.log(values)
);
//output
//The promise rejected with the reason "Done promise3".
```

In this example we get error output why because, the promise3 will execute first so Promise.race() gives rejected output.

It is useful when you want the first async task to complete but do not care it is succeed or fail.
